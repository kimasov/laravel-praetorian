<?php

return [

    'settings' => [

        /**
         * Use custom provider for binding guards and providers
         */
        'custom_provider' => false,

        /**
         * Prefix for all package routes
         */
        'route_prefix' => 'api/v1/auth',

        /**
         * Enable and disable routes
         */
        'routes' => [

            /**
             * Routes for login, refresh token and logout
             */
            'auth' => true,

            /**
             * Service route for test authorization
             */
            'check' => true,

        ],

        /**
         * Name of post request parameter
         * which provide information about which guard should be used
         */
        'guard_parameter_name' => 'guard',

        /**
         * Access token settings
         */
        'access_token' => [

            /**
             * Access token expiration period, minutes
             */
            'expiration' => 5

        ],

        /**
         * Refresh token settings
         */
        'refresh_token' => [

            /**
             * Set refresh token cookie instead of send it in json response
             * Get refresh token from cookie instead of post input
             * true - use cookie
             * false - use json
             */
            'use_cookie' => false,

            /**
             * Refresh token expiration period, minutes
             */
            'expiration' => 1440,

        ],

        /**
         * Password expiration settings
         */
        'password_expiration' => [

            /**
             * Enable password expiration
             */
            'enabled' => false,

            /**
             * Password expiration period, days
             */
            'expiration' => 30,

        ],

        /**
         * Fingerprint check settings
         */
        'fingerprint_check' => [

            /**
             * Enable fingerprint check
             */
            'enabled' => env('PRAETORIAN_FINGERPRINT_CHECK', true),

        ],

    ],


    /**
     * Guards settings
     * merge with config in config/auth.php
     */
    'guards' => [

        'praetorian' => [
            'driver' => 'jwt',
            'provider' => 'users'
        ],

    ],


    /**
     * Providers settings
     * merge with config in config/auth.php
     */
    'providers' => [

        'users' => [
            'driver' => 'orm',
            'model' => \App\Models\User::class,
            'identifier' => 'email',
            'hash' => 'password'
        ],

    ]

];
