<?php
use Illuminate\Support\Facades\Route;
use MDHCode\LaravelPraetorian\Controllers\PraetorianAuthController;

Route::prefix(config('praetorian.settings.route_prefix'))->group(function() {

    Route::post('login', [PraetorianAuthController::class, 'login']);

    Route::post('refresh', [PraetorianAuthController::class, 'refresh']);

    Route::post('logout', [PraetorianAuthController::class, 'logout']);

    Route::post('logout-all', [PraetorianAuthController::class, 'logoutAll']);

});

