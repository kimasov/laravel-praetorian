<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::prefix(config('praetorian.settings.route_prefix'))->middleware(['auth:praetorian'])->group(function() {

    Route::get('check', function() {
        return response(Auth::user());
    });

});
