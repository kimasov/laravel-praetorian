<?php
namespace MDHCode\LaravelPraetorian;

use Illuminate\Support\Facades\Config;
use MDHCode\LaravelPraetorian\Commands\Install;
use MDHCode\LaravelPraetorian\Guard;
use MDHCode\LaravelPraetorian\Providers\Eloquent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class PraetorianServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Install::class,
            ]);
        }

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations/');

        $this->publishes([
            __DIR__.'/../config/praetorian.php' => config_path('praetorian.php')
        ]);

        $this->setConfig();

        $this->bindComponents();

        $this->loadRoutes();
    }

    public function setConfig()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/praetorian.php', 'praetorian');

        Config::set('auth.guards', array_merge(
            config('auth.guards'),
            config('praetorian.guards')
        ));

        Config::set('auth.providers', array_merge(
            config('auth.providers'),
            config('praetorian.providers')
        ));
    }

    public function bindComponents()
    {
        $this->app->bind(TokenService::class, function($app) {
            return new TokenService();
        });

        if(!config('praetorian.settings.custom_provider'))
        {
            Auth::provider('orm', function ($app, array $config) {
                $tokenService = $app->make(TokenService::class);
                return new Eloquent($tokenService, $config['model'], $config['identifier'], $config['hash']);
            });

            Auth::extend('jwt', function ($app, $name, array $config) {
                return new Guard(Auth::createUserProvider($config['provider']), $app->make('request'));
            });
        }
    }

    public function loadRoutes()
    {
        if(config('praetorian.settings.routes.auth'))
            $this->loadRoutesFrom(__DIR__.'/../routes/auth.php');

        if(config('praetorian.settings.routes.check'))
            $this->loadRoutesFrom(__DIR__.'/../routes/check.php');
    }
}