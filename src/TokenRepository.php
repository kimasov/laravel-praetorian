<?php
namespace MDHCode\LaravelPraetorian;

use MDHCode\LaravelPraetorian\Models\RefreshToken;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Carbon;
use MDHCode\LaravelPraetorian\Exceptions;
use Illuminate\Support\Str;

class TokenRepository
{
    const MAX_REFRESH_SESSIONS_COUNT = 5;

    protected $authenticatable;

    public function __construct(Authenticatable $authenticatable)
    {
        $this->authenticatable = $authenticatable;
    }

    /**
     * Create instance from token id
     *
     * @param string $id
     * @return static
     * @throws Exceptions\UserNotFoundException
     * @throws Exceptions\InvalidRefreshTokenException
     */
    public static function createFromTokenId(string $id)
    {
        if($token = RefreshToken::find($id))
        {
            if($token->user)
            {
                return new static($token->user);
            }
            else
            {
                throw new Exceptions\UserNotFoundException();
            }
        }
        else
        {
            throw new Exceptions\InvalidRefreshTokenException('Token expired, or revoked, or never existed', 401);
        }
    }

    /**
     * Return user
     *
     * @return Authenticatable
     */
    public function getUser()
    {
        return $this->authenticatable;
    }

    /**
     * Issue and store new refresh session
     *
     * @return RefreshToken
     * @throws \Exception
     */
    public function issue(): RefreshToken
    {
        $fingerprint = $this->getFingrerprint();

        if($token = $this->authenticatable->refreshTokens()->where('fingerprint', $fingerprint)->first())
        {
            $token->delete();
        }

        $refreshToken = new RefreshToken([
            'refresh_token' => base64_encode(hash('sha512', $this->authenticatable->{$this->authenticatable->getKeyName()}.$fingerprint.Str::uuid()->serialize())),
            'fingerprint' => $fingerprint,
            'expired_at' => Carbon::now()->addMinutes(config('praetorian.settings.refresh_token.expiration')),
        ]);

        $refreshToken->user()->associate($this->authenticatable);

        $refreshToken->save();

        return $refreshToken;
    }

    /**
     * Return browser fingerprint
     *
     * @return string
     */
    public function getFingrerprint()
    {
        $request = request();

        return hash('sha256', implode('|', [
            'user-agent' => $request->headers->get('user-agent'),
            'accept-language' => $request->headers->get('accept-language'),
            'accept-encoding' => $request->headers->get('accept-encoding'),
            'domain' => $request->route()->getDomain()
        ]));
    }

    /**
     * Check token existence and fingerprint is equal to current
     *
     * @param string $token
     * @return bool
     * @throws Exceptions\InvalidRefreshTokenException
     */
    public function check(string $token): bool
    {
        $t = $this->authenticatable->refreshTokens()->where('refresh_token', $token)->first();

        if($t)
        {
            if($t->expired_at > Carbon::now())
            {
                if(!config('praetorian.settings.fingerprint_check.enabled') || $t->fingerprint === $this->getFingrerprint())
                {
                    return true;
                }
                else
                {
                    throw new Exceptions\InvalidRefreshTokenException('Fingerprint is corrupt', 401);
                }
            }
            else
            {
                throw new Exceptions\InvalidRefreshTokenException('Token expired', 401);
            }
        }
        else
        {
            throw new Exceptions\InvalidRefreshTokenException('Token expired, or revoked, or never existed', 401);
        }
    }

    /**
     * Delete one token by id, if it belongs to user
     *
     * @param string $id
     */
    public function delete(string $token): void
    {
        $token = $this->authenticatable->refreshTokens()->where('refresh_token', $token)->first();

        if($token)
        {
            $token->delete();
        }
    }

    /**
     * Delete all tokens for user
     */
    public function deleteAll(): void
    {
        foreach($this->authenticatable->refreshTokens as $token)
        {
            $token->delete();
        }
    }

    /**
     * Determines if user refresh sessions limit exceeded
     *
     * @return bool
     */
    public function checkRefreshSessionsCount(): bool
    {
        return $this->authenticatable->refreshTokens()->where('expired_at', '>', Carbon::now())->count() <= static::MAX_REFRESH_SESSIONS_COUNT;
    }

    /**
     * Determines if user has any active refresh tokens
     *
     * @return bool
     */
    public function hasRefreshSession(): bool
    {
        return $this->authenticatable->refreshTokens()->where('expired_at', '>', Carbon::now())->count() > 0;
    }
}