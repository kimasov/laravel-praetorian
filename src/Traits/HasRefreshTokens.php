<?php
namespace MDHCode\LaravelPraetorian\Traits;

use App\Models\User;
use Illuminate\Support\Carbon;
use MDHCode\LaravelPraetorian\Models\RefreshToken;

trait HasRefreshTokens
{
    public function refreshTokens()
    {
        return $this->morphMany(RefreshToken::class, 'tokenable');
    }

    /**
     * Determines if password expired
     *
     * @return bool
     */
    public function isPasswordExpired()
    {
        if(!config('praetorian.settings.password_expiration.enabled')) return false;

        return Carbon::now() > $this->password_expires_at;
    }

    /**
     * @return Carbon
     */
    public function getPasswordExpiresAtAttribute()
    {
        if($this->password_changed_at)
        {
            $last = Carbon::parse($this->password_changed_at);
        }
        else
        {
            $last = Carbon::parse($this->created_at);
        }

        return $last->addDays(config('praetorian.settings.password_expiration.expiration'));
    }
}
