<?php

namespace MDHCode\LaravelPraetorian\Providers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use MDHCode\LaravelPraetorian\TokenService;

class Eloquent implements UserProvider
{
    protected $tokenService;

    protected $model;

    protected $identifierField;

    protected $passwordField;

    protected $verifyCredentialsClosure;

    protected $retrieveByCredentialsClosure;

    public function __construct(TokenService $tokenService, string $model, string $identifierField = 'email', string $passwordField = 'password')
    {
        $this->tokenService = $tokenService;
        $this->model = $model;
        $this->identifierField = $identifierField;
        $this->passwordField = $passwordField;
    }

    /**
     * @param mixed $identifier
     * @return Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        return $this->model::find($identifier);
    }

    /**
     * Return user by access token
     *
     * @param string $token
     * @return Authenticatable|null
     * @throws \Exception
     */
    public function retrieveByAccessToken(string $token)
    {
        $payload = $this->tokenService->verifyAccess($token);

        return $this->retrieveById($payload->id);
    }

    /**
     * @param mixed $identifier
     * @param string $token
     * @return null
     */
    public function retrieveByToken($identifier, $token)
    {
        return null;
    }

    /**
     * @param Authenticatable $authenticatable
     * @param string $token
     * @return null
     */
    public function updateRememberToken(Authenticatable $authenticatable, $token)
    {
        //
    }

    /**
     * @param array $credentials
     * @return Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        if($this->verifyCredentialsClosure)
        {
            return $this->retrieveByCredentialsClosure->call($this, $credentials);
        }
        else
        {
            if (empty($credentials) || !isset($credentials[$this->identifierField]))
                return null;

            return $this->model::where($this->identifierField, 'ILIKE', $credentials[$this->identifierField])->first();
        }
    }

    /**
     * Set custom function for retrieve by credentials
     *
     * @param \Closure $closure
     */
    public function setRetrieveByCredentialsClosure(\Closure $closure)
    {
        $this->retrieveByCredentialsClosure = $closure;
    }

    /**
     * @param Authenticatable $authenticatable
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $authenticatable, array $credentials)
    {
        if($this->verifyCredentialsClosure)
        {
            return $this->verifyCredentialsClosure->call($this, $authenticatable, $credentials);
        }
        else
        {
            return strtolower($authenticatable->{$this->identifierField}) === strtolower($credentials[$this->identifierField]) && password_verify($credentials[$this->passwordField], $authenticatable->{$this->passwordField});
        }
    }

    /**
     * Set custom function for verify credentials
     *
     * @param \Closure $closure
     */
    public function setVerifyCredentialsClosure(\Closure $closure)
    {
        $this->verifyCredentialsClosure = $closure;
    }
}
