<?php
namespace MDHCode\LaravelPraetorian\Exceptions;

class NoKeyException extends \Exception implements PraetorianException
{
    public function __construct($message = "No key, you must run praetorian:install command", $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

