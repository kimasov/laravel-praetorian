<?php
namespace MDHCode\LaravelPraetorian\Exceptions;

use MDHCode\LaravelPraetorian\Exceptions\PraetorianException;
use Throwable;

class UserNotFoundException extends \Exception implements PraetorianException
{
    public function __construct($message = "User not found", $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}