<?php
namespace MDHCode\LaravelPraetorian\Exceptions;

use MDHCode\LaravelPraetorian\Exceptions\PraetorianException;
use Throwable;

class InvalidRefreshTokenException extends \Exception implements PraetorianException
{
    public function __construct($message = "Invalid refresh token", $code = 401, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}