<?php
namespace MDHCode\LaravelPraetorian\Exceptions;

use MDHCode\LaravelPraetorian\Exceptions\PraetorianException;
use Throwable;

class PasswordExpiredException extends \Exception implements PraetorianException
{
    public function __construct($message = "Password expired", $code = 444, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}