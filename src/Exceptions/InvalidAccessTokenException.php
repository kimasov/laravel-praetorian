<?php
namespace MDHCode\LaravelPraetorian\Exceptions;

use MDHCode\LaravelPraetorian\Exceptions\PraetorianException;
use Throwable;

class InvalidAccessTokenException extends \Exception implements PraetorianException
{
    public function __construct($message = "Invalid access token", $code = 401, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}