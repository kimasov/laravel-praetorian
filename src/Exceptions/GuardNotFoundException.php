<?php
namespace MDHCode\LaravelPraetorian\Exceptions;

class GuardNotFoundException  extends \Exception implements PraetorianException
{
    public function __construct($message = "Guard not found in praetorian", $code = 401, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}