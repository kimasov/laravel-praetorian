<?php
namespace MDHCode\LaravelPraetorian\Exceptions;

use MDHCode\LaravelPraetorian\Exceptions\PraetorianException;
use Throwable;

class RefreshSessionsLimitExceededException extends \Exception implements PraetorianException
{
    public function __construct($message = "Refresh sessions limit exceeded, all tokens revoked", $code = 401, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}