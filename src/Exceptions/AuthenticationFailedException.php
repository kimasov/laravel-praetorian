<?php
namespace MDHCode\LaravelPraetorian\Exceptions;

use MDHCode\LaravelPraetorian\Exceptions\PraetorianException;
use Throwable;

class AuthenticationFailedException extends \Exception implements PraetorianException
{
    public function __construct($message = "Wrong credentials", $code = 401, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}