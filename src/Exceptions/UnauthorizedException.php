<?php
namespace MDHCode\LaravelPraetorian\Exceptions;

use MDHCode\LaravelPraetorian\Exceptions\PraetorianException;
use Throwable;

class UnauthorizedException extends \Exception implements PraetorianException
{
    public function __construct($message = "Unauthorized", $code = 401, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}