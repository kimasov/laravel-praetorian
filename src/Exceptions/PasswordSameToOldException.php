<?php
namespace MDHCode\LaravelPraetorian\Exceptions;

use MDHCode\LaravelPraetorian\Exceptions\PraetorianException;
use Throwable;

class PasswordSameToOldException extends \Exception implements PraetorianException
{
    public function __construct($message = "The password must not be the same as the old one", $code = 422, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}