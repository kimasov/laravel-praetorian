<?php
namespace MDHCode\LaravelPraetorian\Exceptions;

/**
 * Exception can be intercepted in laravel exception handler by this interface
 *
 * Interface PraetorianException
 * @package MDHCode\LaravelPraetorian\Exceptions
 */
interface PraetorianException {

}