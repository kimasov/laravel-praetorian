<?php

namespace MDHCode\LaravelPraetorian\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefreshToken extends Model
{
    use HasFactory;

    protected $primaryKey = 'refresh_token';
    protected $keyType = 'string';

    protected $table = 'refresh_tokens';

    protected $fillable = [
        'refresh_token',
        'fingerprint',
        'expired_at'
    ];

    public function user()
    {
        return $this->morphTo('tokenable');
    }
}
