<?php
namespace MDHCode\LaravelPraetorian;

use Illuminate\Contracts\Auth\Authenticatable;
use Firebase\JWT\JWT;
use Illuminate\Support\Carbon;
use MDHCode\LaravelPraetorian\Exceptions;
use Illuminate\Support\Facades\Cookie;

class TokenService
{
    const TYPE_ACCESS = 'access';
    const TYPE_REFRESH = 'refresh';

    protected $key;
    protected $alg = 'HS256';

    /**
     * @throws Exceptions\NoKeyException
     */
    public function __construct()
    {
        if(!file_exists(storage_path('/praetorian.key')))
            throw new Exceptions\NoKeyException();

        $this->key = str_replace(['-----BEGIN PRIVATE KEY-----', '-----END PRIVATE KEY-----', "\n"], '', file_get_contents(storage_path('/praetorian.key')))."\n";;
    }

    /**
     * Return access token expiration timestamp
     *
     * @return int
     */
    public function getAccessExp(): int
    {
        return Carbon::now()->addMinutes(config('praetorian.settings.access_token.expiration'))->timestamp;
    }

    /**
     * Return refresh token expiration timestamp
     *
     * @return int
     */
    public function getRefreshExp(): int
    {
        return Carbon::now()->addMinutes(config('praetorian.settings.refresh_token.expiration'))->timestamp;
    }

    /**
     * Issue new access token
     *
     * @param Authenticatable $authenticatable
     * @return string
     */
    public function issueAccess(Authenticatable $authenticatable): string
    {
        return $this->issue([
            'id' => $authenticatable->{$authenticatable->getKeyName()},
            'exp' => $this->getAccessExp(),
            'type' => static::TYPE_ACCESS
        ]);
    }

    /**
     * Issue new refresh token
     *
     * @param Authenticatable $authenticatable
     * @return string
     * @throws \Exception
     */
    public function issueRefresh(Authenticatable $authenticatable): string
    {
        $tokenRepository = new TokenRepository($authenticatable);

        $refreshToken = $tokenRepository->issue();

        return $refreshToken->refresh_token;
    }

    /**
     * Delete old refresh token, issue new refresh token
     *
     * @param Authenticatable $authenticatable
     * @param string $refresh_token
     * @return string
     */
    public function refresh(Authenticatable $authenticatable, string $refresh_token): string
    {
        $tokenRepository = new TokenRepository($authenticatable);

        $tokenRepository->delete($refresh_token);

        return $this->issueRefresh($authenticatable);
    }

    /**
     * Create jwt token
     *
     * @param array $payload
     * @return string
     */
    protected function issue(array $payload): string
    {
        return JWT::encode($payload, $this->key, $this->alg);
    }

    /**
     * Extract data from jwt token, check digital signature and exp time
     *
     * @param string $jwt
     * @return object
     */
    protected function decode(string $jwt): object
    {
        return JWT::decode($jwt, $this->key, [$this->alg]);
    }

    /**
     * Check access token is valid
     *
     * @param string $jwt
     * @return object
     * @throws \Exception
     */
    public function verifyAccess(string $jwt)
    {
        try {
            $payload = $this->decode($jwt);
        } catch (\Throwable $e) {
            throw new Exceptions\InvalidAccessTokenException($e->getMessage(), 401);
        }

        if($payload->type !== static::TYPE_ACCESS)
            throw new Exceptions\InvalidAccessTokenException('Refresh token provided, access token expected', 401);

        return $payload;
    }

    /**
     * Check refresh token is valid
     *
     * @param string $jwt
     * @return object
     * @throws \Exception
     */
    public function verifyRefresh(string $jwt)
    {
        try {
            $payload = $this->decode($jwt);
        } catch (\Throwable $e) {
            Cookie::queue(Cookie::forget('refresh_token'));
            throw new Exceptions\InvalidRefreshTokenException($e->getMessage(), 401);
        }

        if($payload->type !== static::TYPE_REFRESH)
            throw new Exceptions\InvalidRefreshTokenException('Access token provided, refresh token expected', 401);

        return $payload;
    }
}