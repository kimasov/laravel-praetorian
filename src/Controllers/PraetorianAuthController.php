<?php

namespace MDHCode\LaravelPraetorian\Controllers;

use App\Http\Controllers\Controller;
use MDHCode\LaravelPraetorian\TokenRepository;
use MDHCode\LaravelPraetorian\TokenService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use MDHCode\LaravelPraetorian\Exceptions;

class PraetorianAuthController extends Controller
{
    const RESPONSE_TYPE_LOGIN = 'login';
    const RESPONSE_TYPE_REFRESH = 'refresh';

    protected $guard;
    protected $guardName;
    protected $praetorianGuards;

    /**
     * @throws Exceptions\GuardNotFoundException
     */
    public function __construct(Request $request)
    {
        $this->praetorianGuards = array_keys(config('praetorian.guards'));

        if(count($this->praetorianGuards) > 1)
        {
            $this->guardName = $request->get(config('praetorian.settings.guard_parameter_name'));
        }
        else
        {
            $this->guardName = $this->praetorianGuards[0];
        }

        if(!in_array($this->guardName, $this->praetorianGuards))
            throw new Exceptions\GuardNotFoundException();

        $this->guard = Auth::guard($this->guardName);
    }

    /**
     * Authenticates user by credentials
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws Exceptions\AuthenticationFailedException
     * @throws Exceptions\RefreshSessionsLimitExceededException
     * @throws Exceptions\PasswordExpiredException
     */
    public function login(Request $request)
    {
        $credentials = $request->all();

        if($this->guard->validate($credentials))
        {
            if($this->guard->user()->isPasswordExpired())
                throw new Exceptions\PasswordExpiredException();

            $tokenRepository = new TokenRepository($this->guard->user());

            if(!$tokenRepository->checkRefreshSessionsCount())
            {
                $tokenRepository->deleteAll();

                throw new Exceptions\RefreshSessionsLimitExceededException();
            }

            return $this->response(static::RESPONSE_TYPE_LOGIN);
        }
        else
        {
            throw new Exceptions\AuthenticationFailedException('Wrong password', 401);
        }
    }

    /**
     * Returns new access token, and renew refresh token
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function refresh()
    {
        try
        {
            $token = $this->guard->getRefreshToken();

            $tokenRepository = TokenRepository::createFromTokenId($token);

            $tokenRepository->check($token);

            $this->guard->setUser($tokenRepository->getUser());

            return $this->response(static::RESPONSE_TYPE_REFRESH, $token);
        }
        catch(\Throwable $e)
        {
            Cookie::queue(Cookie::forget('refresh_token'));
            throw $e;
        }
    }

    /**
     * Return json-response and cookies for login and refresh request
     *
     * @param string $response_type
     * @param string $refresh_token
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    protected function response(string $response_type, string $refresh_token = '')
    {
        $tokenService = new TokenService();

        if($user = $this->guard->user())
        {
            $access_token = $tokenService->issueAccess($user);

            if($response_type == static::RESPONSE_TYPE_LOGIN)
            {
                $refresh_token = $tokenService->issueRefresh($user);
            }
            else if($response_type === static::RESPONSE_TYPE_REFRESH)
            {
                $refresh_token = $tokenService->refresh($user, $refresh_token);
            }

            $response = [
                'access_token' => $access_token,
                'expired_at' => $tokenService->getAccessExp(),
                'type' => 'bearer'
            ];

            if(config('praetorian.settings.refresh_token.use_cookie'))
            {
                $cookie = Cookie::make(
                    'refresh_token',
                    $refresh_token,
                    env('REFRESH_EXP'),
                    '/api/v1/auth',
                    env('APP_DOMAIN'),
                    !env('APP_ENV', 'production') === 'local',
                    true
                );

                Cookie::queue($cookie);
            }
            else
            {
                $response['refresh_token'] = $refresh_token;
                $response['refresh_expired_at'] = $tokenService->getRefreshExp();
            }

            return response($response, 200);
        }
        else
        {
            throw new Exceptions\UnauthorizedException('Not authorized', 401);
        }
    }

    /**
     * Revoke current refresh token
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function logout()
    {
        if(config('praetorian.settings.refresh_token.use_cookie'))
            Cookie::queue(Cookie::forget('refresh_token'));

        $token = $this->guard->getRefreshToken();

        $tokenRepository = TokenRepository::createFromTokenId($token);

        $tokenRepository->check($token);

        $tokenRepository->delete($token);

        return response(['message' => 'token revoked'], 200);
    }

    /**
     * Revoke all refresh tokens for current user
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function logoutAll()
    {
        if(config('praetorian.settings.refresh_token.use_cookie'))
            Cookie::queue(Cookie::forget('refresh_token'));

        $token = $this->guard->getRefreshToken();

        $tokenRepository = TokenRepository::createFromTokenId($token);

        $tokenRepository->check($token);

        $tokenRepository->deleteAll();

        return response(['message' => 'all tokens revoked'], 200);
    }
}
