<?php
namespace MDHCode\LaravelPraetorian;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Guard as GuardInterface;
use Illuminate\Http\Request;
use Illuminate\Auth\GuardHelpers;
use MDHCode\LaravelPraetorian\Exceptions;

class Guard implements GuardInterface
{
    use GuardHelpers;

    protected $request;

    protected $is_checked = false;

    /**
     * Guard constructor.
     * @param UserProvider $provider
     * @param Request $request
     */
    public function __construct(UserProvider $provider, Request $request)
    {
        $this->setProvider($provider);

        $this->request = $request;
    }

    /**
     * Determine if the current user is authenticated.
     *
     * @return bool
     */
    public function check()
    {
        if(!$this->is_checked)
        {
            try {
                $authenticatable = $this->validateAccessToken();

                if($authenticatable)
                {
                    $tokenRepository = new TokenRepository($authenticatable);

                    if($tokenRepository->hasRefreshSession())
                    {
                        $this->setUser($authenticatable);
                    }
                    else
                    {
                        throw new Exceptions\UnauthorizedException('User has no active sessions');
                    }
                }
                else
                {
                    throw new Exceptions\UserNotFoundException();
                }
            } catch (\Throwable $e) {
                return null;
            } finally {
                $this->is_checked = true;
            }
        }

        return ! is_null($this->user());
    }

    /**
     * Verify access token
     *
     * @return Authenticatable | null
     * @throws Exceptions\InvalidAccessTokenException
     */
    protected function validateAccessToken()
    {
        $token = $this->getAccessToken();

        return $this->provider->retrieveByAccessToken($token);
    }

    /**
     * Get token from request headers
     *
     * @return string
     * @throws Exceptions\InvalidAccessTokenException
     */
    public function getAccessToken(): string
    {
        $token =  trim(str_replace('Bearer ', '', $this->request->headers->get('authorization')));

        if(!$token)
            throw new Exceptions\InvalidAccessTokenException('No token provided', 401);

        return $token;
    }

    /**
     * Get refresh token from cookie
     *
     * @return string
     * @throws Exceptions\InvalidRefreshTokenException
     */
    public function getRefreshToken(): string
    {
        if(config('praetorian.settings.refresh_token.cookie_instead_of_response'))
        {
            $token = $this->request->cookie('refresh_token');
        }
        else
        {
            $token = request()->get('refresh_token');
        }

        if(!$token)
            throw new Exceptions\InvalidRefreshTokenException('No token provided', 401);

        return $token;
    }

    /**
     * Return authenticated user
     *
     * @return Authenticatable|null
     */
    public function user()
    {
        return $this->user;
    }

    /**
     * Validate user credentials
     *
     * @param array $credentials
     * @return bool
     * @throws Exceptions\UserNotFoundException
     */
    public function validate(array $credentials = [])
    {
        if($authenticatable = $this->provider->retrieveByCredentials($credentials))
        {
            $this->setUser($authenticatable);

            return $this->provider->validateCredentials($authenticatable, $credentials);
        }
        else
        {
            throw new Exceptions\UserNotFoundException();
        }
    }
}